package com.barsu.realty.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity(name = "realty")
@Getter
@NoArgsConstructor
@ToString
public class Realty {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true,nullable = false)
    @Setter
    private Long numDogovor;

    @Column(unique = true,nullable = false)
    @Setter
    private String nameRealty;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "nameOperation",referencedColumnName = "nameOperation",nullable = false)
    @Setter
    private Operation operation;

    @Column(nullable = false)
    @Setter
    private String client;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "status",referencedColumnName = "status",nullable = false)
    @Setter
    private Status status;

    @Column(nullable = false)
    @Setter
    private Double price;

    public Realty(Long numDogovor, String nameRealty, Operation operation, String client, Status status, Double price) {
        this.numDogovor = numDogovor;
        this.nameRealty = nameRealty;
        this.operation = operation;
        this.client = client;
        this.status = status;
        this.price = price;
    }
}
