package com.barsu.realty.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "operations")
@Getter
@NoArgsConstructor
@ToString
public class Operation implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    @Setter
    private String nameOperation;

    public Operation(String nameOperation) {
        this.nameOperation = nameOperation;
    }
}
