package com.barsu.realty.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "status")
@Getter
@NoArgsConstructor
@ToString
public class Status implements Serializable {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    @Setter
    private String status;

    public Status(String status) {
        this.status = status;
    }
}
