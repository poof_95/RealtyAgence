package com.barsu.realty.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

@SpringUI
public class RealtyUI extends UI{

    private final MyCrud crud;

    public RealtyUI(MyCrud crud) {
        this.crud = crud;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(crud);
    }

}