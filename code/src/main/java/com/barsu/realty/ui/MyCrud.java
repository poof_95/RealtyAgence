package com.barsu.realty.ui;

import com.barsu.realty.models.Realty;
import com.barsu.realty.repository.OperationRepository;
import com.barsu.realty.repository.StatusRepository;
import com.barsu.realty.services.RealtyService;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.impl.field.provider.ComboBoxProvider;
import org.vaadin.crudui.form.impl.form.factory.GridLayoutCrudFormFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/*
 * CrudUI docs : https://vaadin.com/directory/component/crud-ui-add-on
 * */

@SpringComponent
@UIScope
public class MyCrud extends VerticalLayout implements CrudListener<Realty> {

    private final RealtyService realtyService;
    private final OperationRepository operationRepository;
    private final StatusRepository statusRepository;

    private VerticalLayout container = new VerticalLayout();
    private TextField nameFilter = new TextField();
    private ComboBox<String> propFilter = new ComboBox<>();
    private Map<String,String> props = new HashMap<>();

    public MyCrud(RealtyService realtyService, OperationRepository operationRepository, StatusRepository statusRepository) {
        this.realtyService = realtyService;
        this.operationRepository = operationRepository;
        this.statusRepository = statusRepository;
        container.setSizeFull();
        container.setMargin(false);
        addComponent(container);
        addCrud(getCrud());
    }

    private void addCrud(Component crud) {
        VerticalLayout layout = new VerticalLayout(crud);
        layout.setSizeFull();
        layout.setMargin(true);
        container.addComponent(crud);
        crud.setVisible(true);
    }

    private Component getCrud(){
        GridCrud<Realty> crud = new GridCrud<>(Realty.class,this);
        GridLayoutCrudFormFactory<Realty> formFactory = new GridLayoutCrudFormFactory<>(Realty.class, 1,6);

        formFactory.setValidationErrorMessage("Ошибка сохранения/изменения! Проверте правильность данных!");
        crud.setCrudFormFactory(formFactory);

        crud.setRowCountCaption("%d недвижимостей найдено");
        crud.setDeletedMessage("Недвижимость успешно удалена!");
        crud.setSavedMessage("Недвижимость успешно сохранена!");

        props.put("Цене","price");
        props.put("Статусу","status");
        props.put("ФИО Клинта","client");
        props.put("Тип операции","name_operation");
        props.put("Названию недвижимости","name_realty");
        props.put("№ Договра","num_dogovor");

        formFactory.setUseBeanValidation(true);

        formFactory.setErrorListener(e -> Notification.show(e.getMessage(), Notification.Type.TRAY_NOTIFICATION));

        formFactory.setDisabledProperties("id");
        formFactory.setVisibleProperties("numDogovor", "nameRealty", "operation", "client", "status", "price");
        formFactory.setFieldCaptions("№ Договра","Название недвижимости","Тип операции","ФИО Клиента","Статус","Цена");
        formFactory.setCancelButtonCaption("Отменить");
        formFactory.setButtonCaption(CrudOperation.ADD,"Добавить");
        formFactory.setButtonCaption(CrudOperation.UPDATE,"Обновить");
        formFactory.setButtonCaption(CrudOperation.DELETE,"Удалить");

        crud.getGrid().setColumns("numDogovor", "nameRealty", "client", "price");
        crud.getGrid().getColumn("numDogovor").setCaption("№ Договора");
        crud.getGrid().getColumn("nameRealty").setCaption("Название недвижимости");
        crud.getGrid().getColumn("client").setCaption("ФИО Клиента");
        crud.getGrid().getColumn("price").setCaption("Цена");
        crud.getGrid().addColumn(e -> e.getOperation() == null ? "" : e.getOperation().getNameOperation()).setCaption("Тип операции");
        crud.getGrid().addColumn(e -> e.getStatus() == null ? "" : e.getStatus().getStatus()).setCaption("Статус");

        formFactory.setFieldProvider("operation", new ComboBoxProvider<>("Тип операции", operationRepository.findAll(), e -> e.getNameOperation()));
        formFactory.setFieldProvider("status", new ComboBoxProvider<>("Статус",statusRepository.findAll(), e -> e.getStatus()));

        nameFilter.setPlaceholder("фильтр...");
        nameFilter.addValueChangeListener(e -> crud.refreshGrid());
        crud.getCrudLayout().addFilterComponent(nameFilter);

        propFilter.setPlaceholder("Поиск по:");
        propFilter.setItems(props.keySet());
        propFilter.addValueChangeListener(e -> crud.refreshGrid());
        crud.getCrudLayout().addFilterComponent(propFilter);

        Button clearFilters = new Button("Очистить", VaadinIcons.ERASER);
        clearFilters.addClickListener(event -> {
            nameFilter.clear();
            propFilter.clear();
        });
        crud.getCrudLayout().addFilterComponent(clearFilters);

        Button printPage = new Button("Распечатать", VaadinIcons.PRINT);
        printPage.addClickListener(event -> JavaScript.getCurrent().execute("print();"));
        crud.getCrudLayout().addFilterComponent(printPage);

        crud.getCrudLayout().setCaption("Агенство недвижимости");

        return crud;
    }

    @Override
    public Collection<Realty> findAll() {
        return realtyService.findByNameLike(nameFilter.getValue(), props.get(propFilter.getValue()));
    }

    @Override
    public Realty add(Realty domainObjectToAdd) {
        try{
            realtyService.createRealty(domainObjectToAdd);
        }catch (Exception e) {
            Notification.show(e.getMessage());
        }finally {
            return domainObjectToAdd;
        }
    }

    @Override
    public Realty update(Realty domainObjectToUpdate) {
        return add(domainObjectToUpdate);
    }

    @Override
    public void delete(Realty domainObjectToDelete) {
        realtyService.deleteRealty(domainObjectToDelete);
    }

}
