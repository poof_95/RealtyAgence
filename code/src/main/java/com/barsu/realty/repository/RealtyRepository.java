package com.barsu.realty.repository;

import com.barsu.realty.models.Realty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealtyRepository extends JpaRepository<Realty,Long>,RealtyRepositoryCustom {
}
