package com.barsu.realty.repository;

import com.barsu.realty.models.Realty;

import java.util.List;

public interface RealtyRepositoryCustom {
    List<Realty> findByNameLike(String name, String prop);
}
