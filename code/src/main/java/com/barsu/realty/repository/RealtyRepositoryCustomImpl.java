package com.barsu.realty.repository;

import com.barsu.realty.models.Realty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class RealtyRepositoryCustomImpl implements RealtyRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Realty> findByNameLike(String name, String prop) {
        String sql = "SELECT * FROM realtysdb.realty where " + prop + " like '%" + name + "%' ;";
        Query query = entityManager.createNativeQuery(sql,Realty.class);
        return query.getResultList();
    }
}
