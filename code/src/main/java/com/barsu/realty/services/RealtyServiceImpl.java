package com.barsu.realty.services;

import com.barsu.realty.models.Realty;
import com.barsu.realty.repository.RealtyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RealtyServiceImpl implements RealtyService {

    private final RealtyRepository realtyRepository;

    public RealtyServiceImpl(RealtyRepository realtyRepository) {
        this.realtyRepository = realtyRepository;
    }

    @Override
    public void createRealty(Realty realty)throws Exception {
        realtyRepository.saveAndFlush(realty);
    }

    @Override
    public void deleteRealty(Realty realty) {
        realtyRepository.delete(realty);
    }

    @Override
    public void deleteAllRealty() {
        realtyRepository.deleteAll();
    }

    @Override
    public List<Realty> getRealtys() {
        return realtyRepository.findAll();
    }

    @Override
    public List<Realty> findByNameLike(String name, String prop) {
        if (name.isEmpty() || prop.isEmpty())
            return getRealtys();
        else
            return realtyRepository.findByNameLike(name,prop);
    }

}
