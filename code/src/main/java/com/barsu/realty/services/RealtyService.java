package com.barsu.realty.services;

import com.barsu.realty.models.Realty;

import java.util.List;

public interface RealtyService {
    void createRealty(Realty realty)throws Exception;
    void deleteRealty(Realty realty);
    void deleteAllRealty();
    List<Realty> getRealtys();
    List<Realty> findByNameLike(String name, String prop);
}
